using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatatallaNabal
{
    class Battleship
    {
        //Creacion del  tablero :3

        public void Displaytablero(char[,] tablero)
        {
            int fila;
            int Columna;

            Console.WriteLine("  ¦ 0 1 2 3 4 5 6 7 8 9");
            Console.WriteLine("--+--------------------");
            for (fila = 0; fila <= 9; fila++)
            {
                Console.Write((fila).ToString() + " ¦ ");
                for (Columna = 0; Columna <= 9; Columna++)
                {
                    Console.Write(tablero[Columna, fila] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("\n");
        }
    }

    class Jugador
    {
        char[,] tabla = new char[10, 10];
        public int contadorA = 0;
        public int contadorF = 0;
        int x = 0;
        int y = 0;

        public int obtenerCA()
        {
            return contadorA;
        }
        public int obtenerCF()
        {
            return contadorF;
        }
        public void cordenada()
        {
            Console.WriteLine("Escribe la cordenada X");
            string line = Console.ReadLine();
            int value;
            if (int.TryParse(line, out value))
            {
                x = value;
            }
            else
            {
                Console.WriteLine("No es un numero Entero !");
            }

            Console.WriteLine("Escribe la cordenada Y");
            line = Console.ReadLine();
            if (int.TryParse(line, out value))
            {
                y = value;
            }
            else
            {
                Console.WriteLine("No es un numero Entero!");
            }

            try
            {
                if (tabla[x, y].Equals('1'))
                {
                    tabla[x, y] = '0';
                    Console.Clear();
                    Console.WriteLine("Le diste!\r\n");
                    contadorA += 1;
                }
                else
                {
                    tabla[x, y] = 'F';
                    Console.Clear();
                    Console.WriteLine("Fallaste!\r\n");
                    contadorF += 1;
                }
            }
            catch
            {
                Console.Clear();
                Console.WriteLine("Error: Escribe un numero entre 0 y 9)");
            }
        }
        public char[,] obtenertabla()
        {
            return tabla;
        }
        public void pocionflota(int q, int w)
        {
            tabla[q, w] = '1';
        }
        public void Randomize()
        {

            Random r = new Random(2);
           
            pocionflota(1, 2);
            pocionflota(2, 2);
            pocionflota(4, 3);
            pocionflota(4, 4);
            pocionflota(4, 5);
           
           
        }

    }

    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Hola!\r\n\r\n");
            Console.WriteLine("CUal es tu nombre?");
            string name = System.Console.ReadLine();
            Console.WriteLine();
            Battleship b = new Battleship();
            Jugador p = new Jugador();
            p.Randomize();
            while (p.obtenerCA() < 5)
            {
                b.Displaytablero(p.obtenertabla());
                p.cordenada();
            }
            Console.WriteLine("Felicidades, " + name + "! Ganaste!\r\n");
            Console.WriteLine("Tu fallaste: " + p.obtenerCF() + " veces\r\n");;
            System.Console.ReadLine();
        }


    }
}
